// next.config.js
const nextEnv = require('next-env');
const dotEnvLoad = require('dotenv-load');
const withOffline = require('next-offline');

dotEnvLoad();
const withNextEnv = nextEnv();

module.exports = withNextEnv(
  withOffline({
    cssModules: true,
    cssLoaderOptions: {
      importLoaders: 1,
      localIdentName: '[local]___[hash:base64:5]',
    },
  })
);

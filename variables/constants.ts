import { IconProp } from '@fortawesome/fontawesome-svg-core';
import {
  faGithub,
  faGitlab,
  faLinkedin,
} from '@fortawesome/free-brands-svg-icons';
import {
  faEnvelope,
  faFileContract,
  faTasks,
} from '@fortawesome/free-solid-svg-icons';

export const name = 'Lorenzo';
export const surname = 'Calamandrei';
export const fullName = name + ' ' + surname;
export const email = 'calamandrei.lorenzo.work@gmail.com';

type Link = {
  link: string;
  color: string;
  title: string;
  icon: IconProp;
  newWindow?: boolean;
};

export const LINKS: Link[] = [
  {
    link: 'https://gitlab.com/lorenzocalamandrei',
    color: 'coral',
    title: 'GitLab',
    icon: faGitlab,
  },
  {
    link: 'https://github.com/CalamandreiLorenzo',
    color: 'black',
    title: 'GitHub',
    icon: faGithub,
  },
  {
    link: '/projects',
    color: 'gray',
    title: 'Projects',
    icon: faTasks,
    newWindow: false,
  },
  {
    link: 'https://linkedin.com/in/lorenzocalamandrei',
    color: 'cornflowerblue',
    title: 'LinkedIn',
    icon: faLinkedin,
  },
  {
    link: `mailto:${email}`,
    color: 'red',
    title: 'Gmail',
    icon: faEnvelope,
  },
  {
    link:
      'https://drive.google.com/file/d/10T4N3R4O9fzRLVWhkLeATV8Gy6NHUNR5/view',
    color: 'orange',
    title: 'CV',
    icon: faFileContract,
  },
];

export const LOGOS = {
  0: '/img/react-logo.png',
  1: '/img/next-logo.png',
  2: '/img/redux-logo.png',
  3: '/img/php-logo.png',
  4: '/img/laravel-logo.png',
  5: '/img/docker-logo.png',
  6: '/img/node-logo.png',
  7: '/img/git-logo.png',
  8: '/img/html-logo.png',
  9: '/img/css-logo.png',
  10: '/img/js-logo.png',
  11: '/img/ts-logo.png',
  12: '/img/magento-logo.png',
  13: '/img/fastlane-logo.png',
  14: '/img/mysql-logo.png',
  15: '/img/symfony-logo.png',
};

type Project = {
  ogImage: string;
  title: string;
  description: string;
  link: string;
};

export const PROJECTS: Project[] = [
  {
    ogImage: 'https://calamandrei-lorenzo.it/img/og_image.png',
    title: 'Portfolio',
    description: 'This NextJS site',
    link: 'https://calamandrei-lorenzo.it/'
  }
];

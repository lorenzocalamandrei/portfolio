FROM node:11.10

WORKDIR /var/www/html
RUN chown node:node ./
USER node

COPY --chown=node:node ./ /var/www/html
RUN yarn
RUN yarn build
CMD yarn start

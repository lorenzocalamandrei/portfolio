import { NextPage } from 'next';
import Link from 'next/link';
import React from 'react';
import { Fade, Slide } from 'react-awesome-reveal';
import {
  Button,
  Card,
  Col,
  Container as BootstrapContainer,
  Row,
} from 'react-bootstrap';
import Container from '../components/Container/container';
import Links from '../components/Links/links';
import Navbar from '../components/Navbar/navbar';
import { PROJECTS } from '../variables/constants';
import style from './projects.module.scss';

const Projects: NextPage = () => {
  return (
    <>
      <Container>
        <Navbar />
        <BootstrapContainer className="mt-5">
          <Row>
            <Slide direction="left" triggerOnce>
              <div className="ml-3">
                <h1>My Projects</h1>
                <h6>Other Projects will be here soon</h6>
              </div>
            </Slide>
          </Row>
          <Row className="mt-5 justify-content-center">
            <Fade style={{ display: 'flex', flexWrap: 'inherit' }} cascade triggerOnce>

              {PROJECTS.map(project => (
                <Col xs={12} md={6} xl={4} className="justify-self-center mt-2">
                  <Card>
                    <Card.Img
                      variant="top"
                      src={project.ogImage}
                    />
                    <Card.Body>
                      <Card.Title>{project.title}</Card.Title>
                      <Card.Text>{project.description}</Card.Text>
                      <Link
                        href={project.link}
                        prefetch={false}
                      >
                        <a target="_blank">
                          <Button variant="primary">Visit</Button>
                        </a>
                      </Link>
                    </Card.Body>
                  </Card>
                </Col>
              ))}

            </Fade>
          </Row>
        </BootstrapContainer>
      </Container>
      <Row className={style.FloatingLinks}>
        <Links inverse />
      </Row>
    </>
  );
};

export default Projects;

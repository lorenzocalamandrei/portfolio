import '@fortawesome/fontawesome-svg-core/styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { AppProps } from 'next/app';
import React from 'react';
import './_app.scss';

const myApp = ({ Component, pageProps }: AppProps) => {
  return <Component {...pageProps} />;
};

export default myApp;

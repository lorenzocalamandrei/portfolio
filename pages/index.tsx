import { NextPage } from 'next';
import React from 'react';
import { Row } from 'react-bootstrap';
// @ts-ignore
import Sky from 'react-sky';
import Avatar from '../components/Avatar/avatar';
import Container from '../components/Container/container';
import Links from '../components/Links/links';
// import NameComponent from '../components/Name/name';
import { LOGOS } from '../variables/constants';
import style from './index.module.scss';

const Index: NextPage = () => {
  let how = 100;
  let size = '75px';
  if (process.browser && window.innerWidth < 426) {
    how = 50;
    size = '40px';
  }

  return (
    <>
      <Sky images={LOGOS} how={how} time={40} size={size} />
      <Container>
        <div className={style.titleContainer}>
          <Avatar avatarSize={100} />
          <div className={style.title}>
            {/* TODO: Review deEncrypt usage */}
            {/*<NameComponent />*/}
            Lorenzo Calamandrei
          </div>
          <div className={style.titleMobile}>Lorenzo Calamandrei</div>
          <Row className="justify-content-center">
            <Links />
          </Row>
        </div>
      </Container>
    </>
  );
};

export default Index;

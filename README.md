# Lorenzo Calamandrei Portfolio

[![pipeline status](https://gitlab.com/lorenzocalamandrei/portfolio/badges/master/pipeline.svg)](https://gitlab.com/lorenzocalamandrei/portfolio/-/commits/master)

![node version](https://img.shields.io/badge/Node-13.8.0-blue)
![next version](https://img.shields.io/badge/NextJS-9.3.1-brightgreen)
![docker version](https://img.shields.io/badge/Docker_Compose-3.7-blue)
![react version](https://img.shields.io/badge/React-16.13.1-green)

## Prerequisites

- [x] Docker
- [x] Docker Compose

## How to start

Run the docker container with `docker-compose up -d`.
Enter into the container (with: `docker container exec -ti portfolio bash`) and run `yarn` and after `yarn dev`.
Visit: `http://localhost` and you are ready to start.

## Contributors

- Lorenzo Calamandrei <nexcal.dev@gmail.com>

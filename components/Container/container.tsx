import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Header from '../Header/header';
import style from './container.module.scss';

interface Props {
  titlePage?: string;
  children: any;
}

const container = ({ children, titlePage }: Props) => (
  <Container fluid className={style.fullHeight}>
    <Header titlePage={titlePage} />
    <Row className={style.fullHeight}>
      <Col className={[style.fullHeight, style.noPadding].join(' ')}>
        {children}
      </Col>
    </Row>
  </Container>
);

export default container;

import Link from 'next/link';
import React from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import style from './roundLink.module.scss';

interface Prop {
  link: string;
  children: any;
  color: string;
  title: string;
  newWindow?: boolean;
  titlePosition?: 'bottom' | 'top';
}

const roundLink = ({
  link,
  color,
  title,
  children,
  newWindow = true,
  titlePosition = 'bottom',
}: Prop) => {
  const content = (
    <>
      <div className={style.container} style={{ color: color }}>
        {children}
      </div>
      <style jsx>{`
        div:hover {
          border-width: 3px;
          border-color: ${color};
        }
      `}</style>
    </>
  );

  if (!title) {
    if (newWindow) {
      return (
        <a href={link} target="_blank">
          {content}
        </a>
      );
    }

    return (
      <Link href={link}>
        <a>{content}</a>
      </Link>
    );
  }

  return (
    <OverlayTrigger
      placement={titlePosition}
      overlay={<Tooltip id={title}>{title}</Tooltip>}
    >
      <div>
        {newWindow ? (
          <a href={link} target="_blank">
            {content}
          </a>
        ) : (
          <Link href={link}>
            <a>{content}</a>
          </Link>
        )}
      </div>
    </OverlayTrigger>
  );
};

roundLink.defaultProps = {
  prefetch: false,
  titlePosition: 'bottom',
};

export default roundLink;

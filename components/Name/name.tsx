import { useEffect } from 'react';
import { useDencrypt } from 'use-dencrypt-effect';
import {
  name as myName,
  surname as mySurname,
} from '../../variables/constants';

const name = () => {
  const { result, dencrypt: deEncrypt } = useDencrypt();
  useEffect(() => {
    deEncrypt(`${myName} ${mySurname}`);
  });

  return <>{result}</>;
};

export default name;

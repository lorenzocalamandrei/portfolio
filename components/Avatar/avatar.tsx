import React, { useEffect, useState } from 'react';
import { Image, Spinner } from 'react-bootstrap';
import { email, name } from '../../variables/constants';

interface Prop {
  avatarSize?: number;
}

const avatar = ({ avatarSize }: Prop) => {
  let [loading, setLoading] = useState(true);
  let [avatar, setAvatar] = useState('');
  useEffect(() => {
    if (!loading) {
      return;
    }

    const avatarApiUrl = `https://gitlab.com/api/v4/avatar?email=${email}&size=${
      avatarSize ?? 32
    }`;
    (async () => {
      const avatar = await (await fetch(avatarApiUrl)).json();
      setAvatar(avatar.avatar_url);
      setLoading(false);
    })();
  });

  if (loading) {
    return <Spinner animation="grow" />;
  }
  return (
    <Image
      src={avatar}
      roundedCircle
      alt={name}
      onError={() => setAvatar('/img/me.png')}
    />
  );
};

export default avatar;

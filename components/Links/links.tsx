import React from 'react';
import { Fade } from 'react-awesome-reveal';
import { LINKS } from '../../variables/constants';
import Link from './Link/link';

interface LinksProp {
  inverse?: boolean;
}

const links = ({ inverse = false }: LinksProp) => {
  let titlePosition: 'bottom' | 'top' = 'bottom';
  if (inverse) {
    titlePosition = 'top';
  }

  return (
    <Fade style={{ display: 'flex', flexWrap: 'inherit' }} cascade triggerOnce damping={0.2}>
      {LINKS.map((link) => (
        <div>
          <Link
            key={link.link}
            titlePosition={titlePosition}
            link={link.link}
            color={link.color}
            icon={link.icon}
            title={link.title}
            newWindow={link.newWindow ?? true}
          />
        </div>
      ))}
    </Fade>
  );
};

export default links;

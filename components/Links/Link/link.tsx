import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Col } from 'react-bootstrap';
import RoundLink from '../../RoundLink/roundLink';

type LinkProps = {
  link: string;
  color: string;
  title: string;
  icon: IconProp;
  titlePosition: 'bottom' | 'top';
  newWindow: boolean;
};

const link = ({
  link,
  color,
  title,
  icon,
  titlePosition,
  newWindow,
}: LinkProps) => (
  <Col xs={4} sm={2} className="mw-100">
    <RoundLink
      link={link}
      color={color}
      title={title}
      titlePosition={titlePosition}
      newWindow={newWindow}
    >
      <FontAwesomeIcon icon={icon} size="2x" />
    </RoundLink>
  </Col>
);

export default link;
